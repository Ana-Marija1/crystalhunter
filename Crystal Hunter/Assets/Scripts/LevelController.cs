﻿using UnityEngine;

public class LevelController : MonoBehaviour
{
    public GameObject mStory;
    public GameObject mLevel;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Crystal"))
            {
                gameObject.GetComponent<Renderer>().enabled = false;
            }
            mLevel.SetActive(false);
            mStory.SetActive(true);
        }
    }
}
