﻿using UnityEngine;
using Assets.Scripts;

public class MainMenu : MonoBehaviour, IExit
{
    public void ExitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
