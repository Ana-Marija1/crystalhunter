﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Assets.Scripts;

public class PauseMenu : MonoBehaviour, IExit
{
    public GameObject mPauseMenuUI;
    public GameObject mLevel;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
                mPauseMenuUI.SetActive(true);
                mLevel.SetActive(false);   
        }
    }

    private void LoadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
