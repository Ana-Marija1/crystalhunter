﻿using UnityEngine;

public class DontDestroyAudio : MonoBehaviour
{
   private void Awake()
   {
        DontDestroyOnLoad(transform.gameObject);
   }
}
