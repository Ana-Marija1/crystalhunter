﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public float mMoveSpeed = 5f;
    private float mPixelPerUnit = 1.28f;
    public Transform mMovePoint;
    public LayerMask mBorderColliders;
    public LayerMask mRocks;
    public LayerMask mGuides;
    public LayerMask mMonsters;
    public LayerMask mPuddles;
    public LayerMask mCrystals;
    public StepCounter mStepCounter;

    private void Start()
    {
        mMovePoint.parent = null;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, mMovePoint.position, mMoveSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, mMovePoint.position) <= 0.05f)
        {
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f)
            {
                if ((Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mMonsters))
                    && (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mBorderColliders)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mGuides)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mMonsters)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mCrystals)))
                {
                    if (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mMonsters)
                    && (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mBorderColliders)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit * 2, 0, 0), 0.2f, mMonsters)))
                    {
                        Destroy(Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mMonsters));
                        mStepCounter.RemoveStep();
                    }
                }
                else
                {
                    if (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mRocks)
                        || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0), 0.2f, mMonsters))
                    {
                        mStepCounter.RemoveStep();
                    }
                    Move(new Vector3(Input.GetAxisRaw("Horizontal") * mPixelPerUnit, 0, 0));
                }
            }

            else if (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f)
            {
                if ((Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mMonsters))
                    && (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mBorderColliders)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mGuides)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mMonsters)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mCrystals)))
                {
                    if (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mMonsters)
                    && (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mBorderColliders)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mRocks)
                    || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit * 2, 0), 0.2f, mMonsters)))
                    {
                        Destroy(Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mMonsters));
                        mStepCounter.RemoveStep();
                    }
                }
                else
                {
                    if (Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mRocks)
                        || Physics2D.OverlapCircle(mMovePoint.position + new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0), 0.2f, mMonsters))
                    {
                        mStepCounter.RemoveStep();
                    }
                    Move(new Vector3(0, Input.GetAxisRaw("Vertical") * mPixelPerUnit, 0));
                }
            }
        }

    }

    private void Move(Vector3 vector3)
    {
        Vector3 newPosition = mMovePoint.position + vector3;

        if (!Physics2D.OverlapCircle(newPosition, 0.2f, mBorderColliders))
        {
            mStepCounter.RemoveStep();    
            mMovePoint.position = newPosition;
            
        } 
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Puddle"))
        {
            mStepCounter.RemoveStep();
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Puddle"))
        {
            mStepCounter.RemoveStep();
        }
    }
}
