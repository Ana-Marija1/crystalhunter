﻿using UnityEngine;
using UnityEngine.Audio;

public class VolumeSettings : MonoBehaviour
{
    public AudioMixer mAudioMixer;
    
    private void SetVolume(float volume)
    {
        mAudioMixer.SetFloat("volume", Mathf.Log10(volume) * 20);
    }
}
