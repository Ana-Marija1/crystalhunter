﻿namespace Assets.Scripts
{
    public interface IExit
    {
        void ExitGame();
    }
}
