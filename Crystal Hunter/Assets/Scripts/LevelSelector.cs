﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
   private void LoadScene(string sceneName)
   {
        SceneManager.LoadScene(sceneName);
   }
}
