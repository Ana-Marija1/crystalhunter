﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts;

public class StoryController : MonoBehaviour, IExit
{
    public Text mStoryText;

    private void SetStory()
    {
        string filePath = Application.streamingAssetsPath + "/Recall_Chat/" + "GameStory" + ".txt";
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;

        try
        {
            using (StreamReader inputFile = new StreamReader(filePath))
            {
                for(int i = 1; i < sceneIndex + 1; i++)
                {
                    inputFile.ReadLine();
                }
                mStoryText.text = inputFile.ReadLine();
            }
        }
        catch (IOException e)
        {
            Debug.LogError("Error reading from file.");
            Debug.LogError(e.Message);
        }
    }
    
    private void Update()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (gameObject.activeInHierarchy && sceneIndex != 9)
        {
            SetStory();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(sceneIndex != 9)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex
                    + 1);
            }
            else
            {
                ExitGame();
            }
        }
    }

    public void ExitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
